fetch('./data.json')
.then(response => response.json())
.then(data => {
      // console.log(data);
      
      
      
      // funzione per popolare i filtri delle categorie
      function populateCategoriesFilter(dataInput) {
            let categories = Array.from(new Set(data.map(el => el.category))).sort()
            let categoryFilterWrapper = document.querySelector('#categoryFilterWrapper')                        
            
            categories.forEach(category => {
                  let div = document.createElement('div')
                  div.classList.add('form-check')
                  div.innerHTML = 
                  `
                  <input class="form-check-input" type="radio" name="categories" id="${category}">
                  <label class="form-check-label" data-category="${category.toLowerCase()}" for="${category}">
                  ${category}
                  </label>
                  
                  `
                  categoryFilterWrapper.appendChild(div)
            })
            
            let all = document.createElement('div')
            all.classList.add('form-check', 'mt-3')
            all.innerHTML = 
            `
            <input class="form-check-input" type="radio" name="categories" id="tutte">
            <label class="form-check-label" data-category="tutte" for="tutte">
            Tutte
            </label>
            
            `
            categoryFilterWrapper.appendChild(all)
      }
      populateCategoriesFilter(data)
      
      // funzione per filtrate per categoria
      function filterByCategory(dataInput) {
            let clickable = document.querySelectorAll('[data-category]')
            
            clickable.forEach(el => {
                  el.addEventListener('click', ()=> {
                        let choosenCategory = el.dataset.category
                        
                        if (choosenCategory == "tutte") {
                              showProducts(data)
                        } else {
                              let filteredProducts = dataInput.filter(el => el.category.toLowerCase() == choosenCategory)
                              showProducts(filteredProducts)                                          
                        }
                        
                        
                        
                  })
            })
            
      }
      filterByCategory(data)
      
      // 2 funzioni per lanciare il filtro del prezzo
      function populatePriceFilter(dataInput) {
            // prendo l'id dell'elemento maxPrice
            let maxPrice = document.querySelector('#maxPrice')
            
            let choosenPrice = document.querySelector('#choosenPrice')
            
            // prendo il numero più alto all'interno del json
            let max = Math.max(...dataInput.map(el => Number(el.price)))
            // console.log(max)
            
            maxPrice.innerText = max + "€"
            
            let sliderPrice = document.querySelector('#sliderPrice')
            sliderPrice.max = max
            sliderPrice.min = 0
            sliderPrice.value = max
            choosenPrice.innerText = max + "€"
      }
      populatePriceFilter(data)
      
      function filterByPrice(dataInput) {
            let choosenPrice = document.querySelector('#choosenPrice')
            let sliderPrice = document.querySelector('#sliderPrice')
            sliderPrice.addEventListener('input', ()=> {
                  choosenPrice.innerText = sliderPrice.value + "€"
                  
                  let filteredProducts = dataInput.filter(el => Number(el.price) <= sliderPrice.value)
                  
                  // console.log(filteredProducts)
                  
                  showProducts(filteredProducts)
                  
            })
      }
      filterByPrice(data)
      
      // funzione per lanciare il filtro per parola
      function filterByWord(dataInput) {
            let filterWord = document.querySelector('#filterWord')
            filterWord.addEventListener('input', ()=> {
                  if(filterWord.value.length > 2) {
                        let filteredProducts = dataInput.filter(el => el.name.toLowerCase().includes(filterWord.value.toLowerCase()))
                        
                        showProducts(filteredProducts)
                  } else {
                        showProducts(dataInput)
                  }
            })
      } 
      filterByWord(data)
      
      // funzione per mostrare i prodotti
      function showProducts(dataInput) {
            let wrapperProducts = document.querySelector('#wrapperProducts')
            wrapperProducts.innerHTML = ""
            dataInput.forEach(el => {
                  let col = document.createElement('div')
                  col.classList.add('col-12', 'col-md-6', 'col-lg-4', 'mb-5')
                  col.innerHTML = 
                  `
                  <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="https://picsum.photos/200/${100 + el.id}" alt="Card image cap">
                  <div class="card-body ">
                  <h5 class="card-title">${el.name.split(" ")[0]}</h5>
                  <p class="card-text">${el.category}</p>
                  <p class="card-text">${el.price}€</p>
                  <a href="#" class="btn button text-main" style="box-shadow: none" ><i class="fas fa-arrow-alt-circle-right"></i></a>
                  </div>
                  </div>
                  
                  `
                  wrapperProducts.appendChild(col)
            })
      }
      showProducts(data)
      
      
})